Building the “FAQ!” application developed in **AngularJS**,**HTML**,**CSS**.

“FAQ!” is an app more like the famous TV show “Who wants to be a millionaire” but a bit different. Here a user will be presented with predefined question. User will have to answer the question in limited amount of time and grab the glorious prize of “NOTHING”. I’ve build the app from scratch following some patterns and practices in client side.

![FAQ.png](https://bitbucket.org/repo/Rk5EKk/images/2439493225-FAQ.png)

### How to Play ###

![How-TO.png](https://bitbucket.org/repo/Rk5EKk/images/3311112145-How-TO.png)

### About page with network capture ###

![About-with network.png](https://bitbucket.org/repo/Rk5EKk/images/1544341614-About-with%20network.png)


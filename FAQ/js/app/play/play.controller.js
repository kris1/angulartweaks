
(function () {
    'use strict';

    angular
        .module('faq')
        .controller('PlayController', PlayController);

    PlayController.$inject = ['$timeout'];

    function PlayController($timeout) {
        var vm = this;

        var TIMER_MAX_VALUE = 5;

        vm.timerValue = 0;
        vm.isTimerStopped = false;
        vm.isAnswered = false;
        vm.isRightAnswer = false;

        vm.timer = timer;
        vm.giveAnswer = giveAnswer;

        function giveAnswer(ans) {
            vm.selectedAnswer = ans;
            vm.isAnswered = true;
            vm.isRightAnswer = ans.result;
        }

        function timer() {
            $timeout(function () {
                if (vm.isAnswered || (vm.timerValue === TIMER_MAX_VALUE)) {
                    vm.isTimerStopped = true;
                } else {
                    vm.timerValue++;
                    timer();
                }
            }, 1000);
        }

        activate();

        function activate() {
            vm.question = "What is capital of new zealand ?";

            vm.answers = [{
                    "answer": "Auckland",
                    "result": false
                },
                {
                    "answer": "Hamilon",
                    "result": false
                },
                {
                    "answer": "Wellington",
                    "result": true
                },
                {
                    "answer": "Christchurch",
                    "result": false
                }];

            vm.timer();
        }
    }
})();